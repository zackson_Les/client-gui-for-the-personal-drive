package in.gui.client;

import java.awt.FileDialog;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

public class thefile {
	public static String newFileDialog() {
		JFrame f = new JFrame();
		f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		FileDialog die = new FileDialog(f, ""); 
		die.setMode(FileDialog.LOAD);
		die.setVisible(true);
		String File = die.getDirectory() + " " + die.getFile();
		System.out.println(File);
		return File;
	}
}
