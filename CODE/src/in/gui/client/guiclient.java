package in.gui.client;

import in.gui.client.thefile;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;


public class guiclient extends JFrame{
	
	public static final void main(String[] args) {
		new guiclient("localhost", 42069);
	}
	
	JButton file = new JButton("Upload");
	JButton filenoenc = new JButton("Upload - Without Encryption");
	JButton DownloadBut = new JButton("Download");
	JButton Exit = new JButton("Exit");
	JPanel p = new JPanel();
	String fi = "";
	JList<String[]> list;
	public guiclient(String ip, int socket) {
		DownloadBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//send dir commend to server, set a list model.
				new filespane(ip, socket);
			}
		});
		
		Exit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Clientsendget.stop(ip,socket);
					System.exit(0);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		
		file.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Clientsendget.accept(thefile.newFileDialog(), ip, socket);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		
		filenoenc.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Clientsendget.acceptno(thefile.newFileDialog(), ip, socket);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setBounds(600, 400, 600, 400);
		p.add(file);
		p.add(filenoenc);
		p.add(DownloadBut);
		p.add(Exit);
		p.setVisible(true);
		this.add(p);
		this.pack();
		this.setVisible(true);
	}

}
