package in.gui.client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Start extends JFrame{
	
	public static void main(String[] args) {
			new Start();
	}
	
	JTextField ipsocket = new JTextField(20);
	JButton con = new JButton("Connect");
	JPanel p = new JPanel();
		public Start() {
			p.add(ipsocket, 0);
			p.add(con,-1);
			p.setSize(100, 200);
			this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			this.add(p);
			this.setSize(400, 100);
			this.setVisible(true);
			con.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					String a[] = ipsocket.getText().split(":");
					String ip = a[0];
					int soc = Integer.parseInt(a[1]);
					//p.setVisible(false);
					Start.super.setVisible(false);
					new guiclient(ip,soc);
			}
		});
	}
}
