package in.gui.client;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import in.ArciveDrive.Arcive;
import in.ArciveDrive.back_to_file;

public class Clientsendget {
	
	public static String[] dir(String ip, int socket1) throws IOException {
		Socket socket = new Socket(ip, socket1);
		OutputStream outputS = socket.getOutputStream();
		InputStream inputS = socket.getInputStream();
		BufferedReader bufferdIn = new BufferedReader(new InputStreamReader(inputS));
		String line;
		outputS.write(("dir\n").getBytes());
		String out = "";
		String g = "";
		while((line = bufferdIn.readLine()) != null) {
			out = line;
			break;
		}
		String b = out.replace(" " , "");
		String[] ab = b.split(",");
		outputS.write("stop\n".getBytes());
		return ab;
	}
	
	public static String filetoString(String file) {
		String input = "";
		String filename = file;
		try {
			//int boom = inputStream.read();
			byte[] eve = Files.readAllBytes(Paths.get(filename));
			input = Base16Encoder.encode(eve);
			for (byte outi : eve) {
				//System.out.println(outi);
			}
		
		}catch(IOException ex2) {
			System.out.println("Error reading the file.");
		}
		return input;
	}
	
	public static String ARtoString() {
		String input = "";
		String filename = "AR.enA";
		try {
			//int boom = inputStream.read();
			List<String> eve = Files.readAllLines(Paths.get(filename));
			//input = Base16Encoder.encode(eve);
			for (String outi : eve) {
				input += outi;
			}
		
		}catch(IOException ex2) {
			System.out.println("Error reading the file.");
		}
		return input;
	}
	
	public static void accept(String filetosend, String ip, int soct) throws UnknownHostException, IOException {
		Socket socket = new Socket(ip, soct);
		OutputStream outputS = socket.getOutputStream();
		//System.out.println("1");
		String[] arr = filetosend.split(" ");
		String name = arr[1];
		Arcive.main(arr[1], arr[0]);
		String mid = "*|*" + ARtoString();
		outputS.write(("accept" + " " + name + " " + mid + "\n").getBytes());
		System.out.println("The File Was Sent");
		outputS.write("stop\n".getBytes());
	}
	
	public static void acceptno(String filetosend, String ip, int soct) throws UnknownHostException, IOException {
		Socket socket = new Socket(ip, soct);
		OutputStream outputS = socket.getOutputStream();
		//System.out.println("1");
		String[] arr = filetosend.split(" ");
		String name = arr[1];
		String mid = filetoString(arr[0]+arr[1]);
		outputS.write(("accept" + " " + name + " " + mid + "\n").getBytes());
		System.out.println("The File Was Sent");
		outputS.write("stop\n".getBytes());
	}
	
	public static void get(String file, String ip , int sock) throws UnknownHostException, IOException {
		Socket socket = new Socket(ip, sock);
		OutputStream outputS = socket.getOutputStream();
		InputStream inputS = socket.getInputStream();
		BufferedReader bufferdIn = new BufferedReader(new InputStreamReader(inputS));
		String line;
		String mod = file;
		outputS.write(("get " + mod + "\n").getBytes());
		while((line = bufferdIn.readLine()) != null) {
			String[] cmd = line.split(" ");
			if(cmd[0].equalsIgnoreCase("Write")) {
				String mdio[] = cmd[1].split("==");
				int con = mdio[0].length();
				int aba = Integer.parseInt(mdio[1]);
				if(pecketch(con, aba)) {
					stringtofile(mdio[0], mod);
					System.out.println("The File Has Been Wirtten.");
				}else {
					outputS.write(("get " + mod + "\n").getBytes());
				}
				outputS.write("stop\n".getBytes());
				break;
			}
		}
	}

	public static void stringtofile(String tofile , String filename) {
		
		//System.out.println(tofile);

		if (tofile.substring(0, 3).equals("*|*")) {
			System.out.println("1");
			try {
				FileOutputStream out = new FileOutputStream("AR.enA");
				out.write(tofile.replace("*|*", "").getBytes());
				out.flush();
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			back_to_file.mn();
		}else {
			System.out.println("2");
			try {
				String file = Downloaddie.newFileDialog(filename) + filename;
				FileOutputStream out = new FileOutputStream(file);
				out.write(Base16Encoder.decode(tofile));
				out.flush();
				out.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	public static boolean pecketch(int main, int tocheck) {
		if(main == tocheck) {
			return true;
		}
		return false;
	}
	
	public static int packetcheck(String thefile) {
		int nob = 0;
		for (int i = 0; i < thefile.length(); i++) {
			nob++;
		}
		return nob;
	}

	public static void stop(String ip, int sock) throws UnknownHostException, IOException {
		Socket socket = new Socket(ip, sock);
		OutputStream outputS = socket.getOutputStream();
		
		outputS.write("stop\n".getBytes());
		
	}
}
