package in.gui.client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;

public class filespane extends JPanel{

	public filespane(String ip, int socket) {
		String arr[];
		try {
			arr = Clientsendget.dir(ip, socket);
			JList list = new JList(arr); //data has type Object[]
			list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
			list.setLayoutOrientation(JList.HORIZONTAL_WRAP);
			list.setVisibleRowCount(-1);

			JScrollPane listScroller = new JScrollPane(list);
			listScroller.setPreferredSize(new Dimension(250, 80));
			this.add(listScroller);
			this.setLayout(getLayout());
			JFrame f = new JFrame();
			f.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			f.getContentPane().add(this, BorderLayout.CENTER);
			f.setSize(300, 200);
			f.setVisible(true);
			list.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if(e.getClickCount() > 1) {
			String mid = list.getSelectedValue().toString();
			
				try {
					Clientsendget.get(mid, ip, socket);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
					}
				}
			});
			
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}

